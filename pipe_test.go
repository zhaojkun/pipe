package pipe

import (
	"context"
	"testing"
	"time"

	"github.com/eapache/queue"
)

func BenchmarkPipe0(b *testing.B) {
	input := make(chan interface{}, 2)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		input <- 1
		<-input
	}
}
func BenchmarkPipe(b *testing.B) {
	input := make(chan interface{})
	output := make(chan interface{})
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	NewInfinitePipe(ctx, input, output)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		input <- 1
		<-output
	}
}

func BenchmarkPipe2(b *testing.B) {
	input := make(chan interface{})
	output := make(chan interface{})
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	NewInfiniteChannel(ctx, input, output)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		input <- 1
		<-output
	}
}

func TestPipe(t *testing.T) {
	input := make(chan interface{})
	output := make(chan interface{})
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	ic := NewInfinitePipe(ctx, input, output)

	input <- 1
	input <- 2
	input <- 3
	input <- 4
	input <- 5
	input <- 6
	go func() {
		for val := range output {
			_ = val
			//fmt.Println(val)
		}
	}()
	time.Sleep(time.Second)
	//	fmt.Println("awake")
	ic.Close()
	time.Sleep(time.Second)
	//fmt.Println("awake")
}

func TestPipe2(t *testing.T) {
	input := make(chan interface{})
	output := make(chan interface{})
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	ic := NewInfiniteChannel(ctx, input, output)

	input <- 1
	input <- 2
	input <- 3
	input <- 4
	input <- 5
	input <- 6
	go func() {
		for val := range output {
			_ = val
			//		fmt.Println(val)
		}
	}()
	time.Sleep(time.Second)
	//fmt.Println("awake")
	ic.Close()
	time.Sleep(time.Second)
	//	fmt.Println("awake")
}

// InfiniteChannel implements the Channel interface with an infinite buffer between the input and the output.
type InfiniteChannel struct {
	ctx           context.Context
	cancel        context.CancelFunc
	input, output chan interface{}
	length        chan int
	buffer        *queue.Queue
}

func NewInfiniteChannel(ctx context.Context, input, output chan interface{}) *InfiniteChannel {
	ctx, cancel := context.WithCancel(ctx)
	ch := &InfiniteChannel{
		ctx:    ctx,
		cancel: cancel,
		input:  input,
		output: output,
		length: make(chan int),
		buffer: queue.New(),
	}
	go ch.infiniteBuffer()
	return ch
}

func (ch *InfiniteChannel) Len() int {
	return <-ch.length
}

func (ch *InfiniteChannel) Close() {
	ch.cancel()
}

func (ch *InfiniteChannel) infiniteBuffer() {
	var input, output chan interface{}
	var next interface{}
	input = ch.input
	defer close(ch.length)
	for input != nil || output != nil {
		select {
		case <-ch.ctx.Done():
			return
		case elem, open := <-input:
			if open {
				ch.buffer.Add(elem)
			} else {
				input = nil
			}
		case output <- next:
			ch.buffer.Remove()
		case ch.length <- ch.buffer.Length():
		}

		if ch.buffer.Length() > 0 {
			output = ch.output
			next = ch.buffer.Peek()
		} else {
			output = nil
			next = nil
		}
	}
}
