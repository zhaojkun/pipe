package pipe

import (
	"context"
	"reflect"

	"github.com/eapache/queue"
)

// InfiniteChannel implements the Channel interface with an infinite buffer between the input and the output.
type InfinitePipe struct {
	ctx           context.Context
	cancel        context.CancelFunc
	input, output interface{}
	length        chan int
	buffer        *queue.Queue
}

func NewInfinitePipe(ctx context.Context, input, output interface{}) *InfinitePipe {
	ctx, cancel := context.WithCancel(ctx)
	ch := &InfinitePipe{
		ctx:    ctx,
		cancel: cancel,
		input:  input,
		output: output,
		length: make(chan int),
		buffer: queue.New(),
	}
	go ch.infiniteBuffer()
	return ch
}

func (ch *InfinitePipe) Len() int {
	return <-ch.length
}

func (ch *InfinitePipe) Close() {
	ch.cancel()
}

func (ch *InfinitePipe) infiniteBuffer() {
	defer close(ch.length)
	doneRe := reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(ch.ctx.Done())}
	inputRe := reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(ch.input)}
	outputRe := reflect.SelectCase{Dir: reflect.SelectSend, Chan: reflect.ValueOf(ch.output)}
	lengthRe := reflect.SelectCase{Dir: reflect.SelectSend, Chan: reflect.ValueOf(ch.length)}
	cases := make([]reflect.SelectCase, 0, 4)
	var output, next interface{}
	input := ch.input
	for {
		lengthRe.Send = reflect.ValueOf(ch.buffer.Length())
		cases = append(cases, doneRe, lengthRe)
		if input != nil {
			cases = append(cases, inputRe)
		}
		if output != nil {
			outputRe.Send = reflect.ValueOf(next)
			cases = append(cases, outputRe)
		}
		chosen, recv, recvOK := reflect.Select(cases)
		switch cases[chosen] {
		case doneRe:
			return
		case inputRe:
			if recvOK {
				ch.buffer.Add(recv.Interface())
			} else {
				input = nil
			}
		case outputRe:
			ch.buffer.Remove()
		}
		if ch.buffer.Length() > 0 {
			output = ch.output
			next = ch.buffer.Peek()
		} else {
			output = nil
			next = nil
		}
		cases = cases[:0]
	}
}
